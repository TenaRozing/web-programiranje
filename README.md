# Web shop za odjeću SportsClothes


## Opis aplikacije

Web shop za odjeću SportsClothes je web aplikacija koja predstavlja web trgovinu za kupovinu odjeće. Aplikacija omogućuje, prije svega, registraciju, odnosno prijavu korisnika te dodavanje proizvoda u košaricu i njihovu kupovinu te pregled narudžbi koje je korisnik napravio. 

Unutar aplikacije kreirane su komponente potrebne da bi aplikacija imala atribute web trgovine. 

Aplikacija ima tri vrste korisnika: Gost, Korisnik i Administrator. Svaki od navedenih korisnika ima drugačije mogućnosti i različita ograničenja.

Gost ima mogućnost registracije kako bi mogao dodavati stavke u košaricu, obavljati kupovinu i naručivati artikle. 

Korisnik je registriani korisnik koji ima mogućnost prijave kako bi mogao dodavati artikle u košaricu, manipulirati istima te naručivati.

Administrator ima najveći broj mogućnosti te može uz sve navedeno za Korisnika i Gosta, još upravljati korisnicima i artiklima te mijenjati njihove podatke. 

Svi korisnici imaju mogućnost traženja određenih proizvoda u *search baru* na stranici s artiklima te se svi imaju mogućnost registrirati, odnosno prijaviti te pregledavati asortiman i sadržaj stranice. 

Početna stranica je glavna podstranica na aplikaciji, ali svaka druga podstranica pruža najvažnije mogućnosti kako bi bila funkcionalna. Na samom vrhu svake, pa tako i početne stranice nalazi se fiksirana navigacijska traka, odnosno scrollanjem prema dolje uvijek je vidljiva korisniku te se ne treba vraćati na vrh kako bi joj se pristupilo. Također na početnoj stranici, ali i na stranici s asortimanom nalazi se gumb povratka na vrh koji se nakon određenog scrollanja pojavi i služi za povratak na vrh podstranice. Neprijavljeni korisnik, točnije Gost u navigacijskoj traci ima ponuđene samo opcije "Pregled asortimana" i "Registracija" unutar koje se nalazi link "Login" koji vodi na podstranicu za prijavu. 

Na početnoj stranici, kao i na svim ostalim podstranicama, unutar navigacijske trake može se primjetiti koji korisnik je prijavljen, odnosno posjećuje stranicu. S povećanjem uloge povećava se i broj mogućnosti koje korisnik bira na navigacijskoj traci. Navigacijska traka nalazi se na svakoj stranici kao i footer u kojem se nalaze linkovi koji vode na podstranice: O nama, Kontakt, Dostava te Povrat i zamjena.

Unutar svake podstranice nalazi se ili breadcrumb koji prikazuje na kojoj podstranici se trenutno nalazi korisnik te sadrži link Početna koji vodi na Početnu stranicu ili je povratak na istu omogućen klikom na gumb Početna. Treća je opcija povratak u natrag klikom na strijelicu u web pregledniku. 

## Ograničenja

- Gosti (ne-ulogirani) korisnici imaju read-only pristup svom funkcionalnom sadržaju web aplikacije s time da imaju mogućnost registracije/prijave. Ne mogu dodavati proizvode u košaricu, manipulirati njima (mijenjati količinu, brisati ih i dodavati iz, i u košaricu), ali vide ponuđene artikle za kupovinu te mogu pretraživati iste u search baru.

- Registrirani/prijavljeni korisnici imaju veće mogućnosti od ne-ulogiranih. Mogu dodavati proizvode u košaricu, manipulirati s njima na način da mijenjaju količinu, zabilježavaju količinu, kupuju te pregledavaju narudžbe. Na posljetku se mogu i odjaviti te tada imaju read-only pristup sve dok se ponovno ne prijave.

- Administrator može uređivati sve i vidjeti sve te ima najveće privilegije. Osim svih ponuđenih mogućnosti Gosta i Korisnika, Administrator u opciji Dashboard koja se pojavljuje samo njemu u navigacijskoj traci, može upravljati korisnicima i artiklima unutar svog sučelja. 

## Persone

Persone predstavljaju tipove korisnika iz perspektive dizajna sučelja. Ovisno koji je cilj određene persone i na kojem dijelu aplikacije se nalaze, mijenjaju se njihove ovlasti i motivi.

- Gost: osoba koja posjećuje web aplikaciju bez da je prijavljena te ima samo read-only pristup bez većih mogućnosti sve dok se ne registrira/prijavi. Do tada može samo pregledavati sadržaj te nema mogućnost dodavanja artikala u košaricu te manipulaciju nad istima. Na podstranici s artiklima ima mogućnost pretraživanja artikala u za to predviđenom search baru. Također, ne može kupovati artikle te pregledavati narudžbe koje je obavio. Još se naziva i Anon. 
- Korisnik: korisnik na stranici koji je registriran tj. ima kreiran korisnički račun. Registrirati se je potrebno samo jednom te se svaki idući puta korisnik prijavljuje s korisničkim imenom i lozinkom koji su pohranjeni u bazi podataka nakon registracije. Korisnik ima mogućnost pregledavanja sadržaja web aplikacije te svih ponuđenih artikala koje može dodavati u košaricu te mijenjati njihovu količinu i uklanjati ih iz košarice. Pri odabiru željenih artikala i količine istih može izvršiti kupnju i vidjeti popis narudžbi koje je obavio. Na kraju se može odjaviti. Nema mogućnost upravljanja na razini baze podataka.
- Admin: Korisnički računi sa admin privilegijama. Imaju pravo pristupa u
Dashboard i mogu mijenjati stvari na razini baze. Ima sve navedene mogućnosti kao Korisnik, te naravno Gost i dodatne. Može upravljati artiklima i korisnicima te mijenjati njihove podatke. 

Svi korisnički računi pohranjeni su u bazu podataka te im je dodjeljena uloga u tablici korisnici. Administrator ima postavljene privilegije unutar sučelja Dashboard gdje upravlja korisnicima i artiklima. Navedene persone nisu odvojeni zapisi u bazi nego se samo razlikuju prema ulozi te se promatraju kao semantički objekti iz perspektive posjetitelja aplikacija.

## Stranice

Osnovne stranice koje se koriste za projekt. Određene stranice su potrebne za implementaciju pojedinih funkcionalnosti kako bi ova web aplikacija zadovoljila svoju svrhu. 

Svaka stranica je podstranica u cjelokupnom projektu, pa je tako i početna stranica samo jedna od podstranica. Prema važnosti je jedna od važnijih, ali ne sadrži količinu funkcionalnosti kao neke druge podstranice (npr. Košarica). Svaka podstranica je zasebna i na nju vodi klik na određeni link ili gumb.

NAPOMENA: Podstranice Registracija i Prijava su različite, ali se u ovom kontekstu mogu promatrati zajedno jer su slični pojmovi te se oboje odnose na pristup većim mogućnostima korisnika. 

- POČETNA stranica - početna stranica projekta. Sadrži navigacijsku traku koja se sastoji od Pregled asortimana i Registracija opcija za korisnika koji nije prijavljen. S prijavom mu se izbor opcija povećava kako je u prethodnom tekstu navedeno. Sadrži carousel s više izmjeničnih slika te prikaz odjeće i zasebne slike koje su zapravo link na pojedine podstranice. Na dnu se nalazi već spomenuti footer.
- PREGLED ASORTIMANA stranica -stranica s ponuđenim artiklima gdje korisnik slobodno pregledava i pretražuje artikle.
- REGISTRACIJA/PRIJAVA stranica - stranica gdje se korisnik registrira ili prijavljuje kako bi mogao imati više mogućnosti u web aplikaciji.
- DASHBOARD stranica - stranica vidljiva samo Administratoru koja sadrži sučelje za upravljanje korisnicima i artiklima. 
- KOŠARICA stranica - stranica sa artiklima koje je korisnik (Korisnik ili Administrator) tamo dodao, ali još nisu kupljeni.
- MOJE NARUDŽBE stranica - stranica koja sadrži popis narudžbi koje je napravio jedan korisnik te se ispisuju za svakog korisnika koji je trenutno prijavljen. Ukoliko korisnik nije prijavljen, nema opciju prikaza te podstranice.
- O NAMA stranica - stranica koja sadrži informacije o web trgovini uz prikazane slike.
- RADNJE stranica - stranica kojoj se pristupa sa stranice O nama i prikazuje fizičke trgovine.
- KONTAKT stranica - stranica s kontaktima za trgovinu.
- DOSTAVA - stranica s informacijama o dostavi artikala
- POVRAT I ZAMJENA stranica - stranica s informacijama vezanim za povrat i zamjenu naručenih artikala.

Svaka stranica koja je navedena smije sadržavati i više funkcionalnosti od navedenih, ako je dizajn aplikacije zamišljen da se određene funkcionalnosti obavljaju sa te stranice. Uz uvjet da ostatak specifikacije ne određuje drugačije.

# User stories

Svaki Epic user story podjeljen je u više user storyja, kojima su definirane funkcionalnosti aplikacije iz perspektive pojedine persone. Svaki user story ima definiran acceptance criteria koji potvrđuje ispunjavanja tog User storyja. Epic user story je ispunjen kada su ispunjeni svi acceptance kriteriji svih storyja tog Epica.

Osim toga, neki user storyji mogu sadržavati polje Need. U njemu su eksplicitno navedeni zahtjevi za funkcionalnost koji se mogu zaključiti iz teksta storyja. Tamo gdje nema polja Need, potrebni resursi za ispunjenje storyja mogu se zdravo-razumski zaključiti.

## Epic 1: Gost može samo pregledavati sadržaj i pretraživati artikle te ima mogućnost registracije/prijave za veće mogućnosti

- S1-1 
  Kao običan Gost koji pristupa HOMEPAGE stranici nemam mogućnosti koristiti njezinu punu funkcionalnost. Mogu se registrirati kao novi korisnik ili prijaviti kao postojeći, nakon čega me sustav redirecta natrag na HOMEPAGE. 

  Need: ime, email, password

  Acceptance criteria: 
  - Gost vidi link na funkcionalnost registracije u navigacijskoj traci
  - Gost može kreirati novi korisnički račun 
  - Gost je redirectan natrag na HOMEPAGE nakon registracije/prijave

- S1-2 
  Kao gost mogu se pristupom na HOMEPAGE stranicu prijaviti u aplikaciju nakon čega me sustav redirecta natrag na HOMEPAGE.

  Need: ispravno ime i password

  Acceptance criteria:
  - Gost vidi link na funkcionalnost za prijavu
  - Gost se može prijaviti u aplikaciju
  - Gost je redirectan nazad na HOMEPAGE nakon prijave

- S1-3
  Kao bilo koji korisnik, mogu pristupiti linku *Pregled asortimana* te pregledavati ponuđene artikle i pretraživati ih u *search baru*

  Acceptance criteria: 
  - Link na *Pregled asortimana* vidljiv je na svim stranicama koje sadrže navigacijsku traku (sve)
  - Klik na link otvara stranicu s artiklima
  - Korisnik može pregledavati artikle
  - Korisnik može pretraživati artikle

# Epic 2: Korisnik može dodavati artikle u košaricu, manipulirati njima, naručivati ih i pregledavati svoje narudžbe. Na kraju se može odjaviti

- S2-1 
  Kao Korisnik(korisnik koji je prjavljen) mogu kliknuti na gumb *Dodaj u košaricu* i vidjeti sadržaj košarice

  Acceptance criteria:
  - Korisnik može pretraživati proizvode nakon upisivanja dijela imena ili marke ili tipa u *search bar* i klikom na gumb   *Pretraži*
  - Korisnik može kliknuti na gumb i dodati proizvod u košaricu
  - Korisnik može vidjeti proizvode dodane u košaricu te njihovu cijenu i količinu
  - Korisnik ima pristup košarici na stranici *Pregled asortimana* i klikom na link *Košarica* u navigacijskoj traci

- S2-2
  Kao korisnik mogu manipulirati artiklima u košarici

  Acceptance criteria: 
  - Korisnik može povećavati količinu artikala u košarici
  - Korisnik može smanjivati količinu artikala u košarici
  - Korisnik može uklanjati artikle iz košarice klikom na gumb *X*
  - Korisnik može zabilježavati količine klikom na gumb *Zabilježi artikle*
  - Korisnik može vidjeti ukupnu cijenu i cijenu pojedinih artikala u košarici

- S2-3 
  Kao korisnik mogu obavati kupnju proizvoda dodanih u košaricu

  Acceptance criteria:
  - Korisnik može kliknuti na gumb *Završi kupovinu*
  - Korisnik će biti preusmjeren na stranicu *Košarica*
  - Korisnik može vidjeti sve artikle u košarici te ih i dalje može ukloniti
  - Korisnik na stranici *Košarica* ne može dodavati artikle u košaricu
  - Korisnik mora popuniti obrazac *Adresa za dostavu* 
  - Korisnik klikom na gumb *Kupi* obavlja kupovinu, ako je prije toga popunio obrazac za dostavu

- S2-4 
  Kao korisnik vidim prikaz da je kupnja izvršena

  Acceptance criteria:
  - Korisnik je nakon klika na gumb *Kupi* redirectan na stranicu *Kupnja izvršena*
  - Korisnik na stranici *Kupnja izvršena* vidi sučelje s naslovom *Hvala na kupnji!* što potvrđuje da je kupnja uspješno obavljena

- S2-5 
  Kao korisnik mogu vidjeti obavljene narudžbe

  Need: obavljena kupovina (klikom na gumb *Kupi*) nakon dodanih proizvoda u košaricu

  Acceptance criteria:
  - Korisnik može klikom na link *Moje narudžbe* u navigacijskoj traci otvoriti stranicu s povijesti svojih narudžbi
  - Korisnik može vidjeti broj i vrijeme narudžbe te ukupnu cijenu i artikle
  - Korsnik može vidjeti artikle određene narudžbe samo klikom na gumb *Prikaži artikle* 
  - Korisniku se zatim prikazuje prozorčić sa brojem narudžbe, nazivom artikala, cijenom, količinom i ukupnom cijenom
  - Korisnik može izaći iz prozorčića klikom na *X* u gornjem desnom uglu

- S2-6 
  Kao Korisnik vidim da sam prijavljen/a i mogu se odjaviti

  Acceptance criteria:
  - Korisnik sve dok je prijavljen u navigacijskoj traci odmah iza logo-a vidi ispis Pozdrav_imeKorisnika prema imenu kojim je registriran
  - Korisnik klikom na gumb *Odjavi me* (navigacijska traka) u bilo kojem trenutku se može odjaviti iz aplikacije i od tada se smatra Gostom i ima njegove mogućnosti sve dok se opet ne prijavi

- S2-7
  Kao Korisnik nemam pristup košarici ako je prazna

  Need: stavke u košarici

  Acceptance criteria:
  - Korisnik klikom na link *Košarica* (navigacijska traka), ako u košarici nema artikala, neće moći pristupiti istoimenoj stranici te će samo bit redirectan na stranicu s koje je izvršio klik na gumb

## Epic 3: Administrator može upravljati korisnicima i artiklima preko sučelja *Admin Dashboard*

- S3-1
  Kao Administrator mogu upravljati korisnicima kada pristupim *Admin Dashboard-u*

  Acceptance criteria:
  - Administrator može na svakoj stranici koja sadrži navigacijsku traku klikom na link *Dashboard* u njoj otvoriti sučelje za upravljanje
  - Administrator je klikom na gumb *Upravljaj korisnicima* preusmjeren na stranicu *Popis korisnika*
  - Administrator vidi popis svih korisnika
  - Administrator vidi ID, ime, email, ulogu i opcije
  - Administrator klikom na gumb *Uredi* ispod *Opcije* bude preusmjeren na stranicu za *Uređivanje korisnika*
  - Administrator tamo ima mogućnost izmjene unaprijed navedenih atributa
  - Administrator ima mogućnost klika na gumb *Uredi* za kraj uređivanja
  - Administrator ima mogućnost klika na gumb *Očisti* za čišćenje atributa koji su postavljeni
  - Administrator se klikom na gumb *Povratak* vraća na stranicu *Popis korisnika*
  - Administrator na stranici *Popis korisnika* klikom na gumb *Obriši* briše korisnika/e
  - Administrator se klikom na gumb *Povratak* ponovno nalazi na stranici *Admin Dashboard*

- S3-2
  Kao Administrator mogu upravljati artiklima kada pristupim *Admin Dashboard-u

  Acceptance criteria:
  - Administrator može na svakoj stranici koja sadrži navigacijsku traku klikom na link *Dashboard* otvoriti sučelje za upravljanje
  - Administrator je klikom na gumb *Upravljaj proizvodima* preusmjeren na stranicu sa svim artiklima u bazi
  - Administrator za svaki artikl vidi ID, marku, naziv, velicinu, cijenu, kolicinu, tip, sliku i opcije s gumbima *Uredi* i *Obriši*
  - Administrator je klikom na gumb *Uredi* preusmjeren na stranicu *Uređivanje proizvoda*
  - Administrator ima mogućnost mijenjati sve navedene atribute nekog artikla te klikom na gumb *Uredi* potvrditi promjenu
  - Administrator klikom na gumb *Resetiraj* uklanja sve napravljene promjene za artikl i postavlja se sve kako je prvotno bilo
  - Administrator se klikom na gumb *Povratak* ponovno nalazi na stranici sa svim artiklima
  - Administrator klikom na gumb *Obriši* briše određeni artikl

- S3-3 
  Kao Administrator imam mogućnost dodavati nove artikle u bazu

  Acceptance criteria: 
  - Administrator je klikom na gumb *Dodaj* na stranici sa popisom svih proizvoda preusmjeren na stranicu *Dodavanje proizvoda*
  - Administrator može dodati novi proizvod
  - Administrator mora popuniti sva polja i dadati sliku artikla
  - Administrator završava dodavanje artikla klikom na gumb *Dodaj* te je proizvod dodan u bazu
  - Administrator mora popuniti sva polja inače će mu se ispisati poruka da određeno polje nije popunjeno
  - Administrator je nakon dodavanja artikla preusmjeren na stranicu sa proizvodima
  - Klikom na gumb *Resetiraj* vraća se sve na prijašnje stanje
  - Administrator se klikom na gumb *Povratak* nalazi na prethodnoj stranici s artiklima

## Epic 4: Administrator ima sve iste mogućnosti kod dodavanja artikala u košaricu, manipuliranja, kupovine i pregleda narudžbi kao i Korisnik

- S4-1 
  Kao Administrator, imam sve iste mogućnosti kao i Korisnik uz još navedene napredne. Sve što može Korisnik može i Administrator, te više.

  Acceptance criteria:
  - Za ostale administratorske ovlasti pročitati Epic 2

## Epic 5: Pregled ostalog sadržaja stranice

- S5-1
  Svi korisnici (Gost, Korisnik i Administrator) imaju mogućnost pregledavanja sadržaja u *footeru*

  Acceptance criteria: 
  - Svi korisnici mogu vidjeti sadržaj na linkovima u *footeru*
  - Svi korisnici klikom na link *O nama* u *footeru* mogu pregledavati sadržaj o trgovini na stranici O nama, na koju su preusmjereni klikom na spomenuti gumb
  - Svi korisnici mogu klikom na link *Kontakt* vidjeti informacije o kontaktima trgovine na istoimenoj stranici
  - Svi korisnici klikom na link *Dostava* mogu vidjeti informacije o dostavi na istoimenoj stranici
  - Svi korisnici klikom na link *Povrat i zamjena* mogu vidjeti informacije o povratu i zamjeni artikala na stranici Povrat i zamjena
  - Svi korisnici klikom na link *Radnje* na stranici *O nama* bit će preusmjereni na stranicu s lokacijama radnji
  - Svi korisnici klikom na link *OVDJE* na stranici *Povrat i zamjena* bit će preusmjereni na internetski link s obrazcem za zamjenu proizvoda

- S5-2 
  Kao bilo koji korisnik mogu pregledavati ostali sadržaj na početnoj stranici (HOMEPAGE)

  Acceptance criteria: 
  - Svi korisnici mogu vidjeti sadržaj početne stranice (HOMEPAGE)
  - Svi korisnici mogu klikom na slike koje su link biti preusmjereni na stranice iz *footera* (Kontakt i Dostava)






