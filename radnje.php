<!DOCTYPE html>
<html lang="en">
<head>
<title>Radnje</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/still.css">

<script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</style>
</head>
<body>

<?php include('navbar.php'); ?>

  <style>
    /* Style the body */
    body {
      font-family: Arial;
      margin: 0;
    }
    
    /* Header/Logo Title */
    .header1 {
    justify-content: center;
    margin-top: 70px;
    }

    .header2 {
        text-align: center;
        padding: 30px;
        background-color: #e6e6e6;
        color: black;
        font-size: 25px;
    }
   
     </style>
    
    <div class="header1">
      <img src="slike/outlet1.jpg" width="100%">
    </div>
    <div class="header2">
        <p>Lokacije SportsClothes trgovina</p>
        <p class="manji">Ovdje potražite lokacije naših trgovina</p1>
    </div>
    <br>
    <a href="index.php" style="margin-left: 120px; color: #800000; text-decoration: none;">POČETNA</a>

    <br><br><br>

    <div class="container">
  
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
         <img src="slike/adidas_osijek.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Adidas shop Osijek - Portanova</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="slike/adidas_varazdin.jpg" alt="Nature" style="width:100%">
           <div class="caption">
                <p>Adidas shop Varaždin - ulica Lipa 13</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="slike/adidas_split.jpg" alt="Fjords" style="width:100%">
           <div class="caption">
             <p>Adidas shop Split - Marontova 23</p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <br>
   <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="slike/pula.jpg" alt="Lights" style="width:100%">
           <div class="caption">
              <p>Ferivi Pula - ulica Sergijevaca 28</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
          <img src="slike/adidas_sibenik.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <p>Ferivi Šibenik - Osječka 256</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
            <img src="slike/ferivi_vinkovci.jpg" alt="Fjords" style="width:100%">
          </div>
          <div class="caption">
           <p>Ferivi Vinkovci - Duga ulica 47</p>
          </div>
        </a>
      </div>
    </div>
    
          </div>
          </div>

          <br>
          
  
          <footer class="footer-distributed">

            <div class="footer-right">
        
              <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
              <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>
              
        
            </div>
        
            <div class="footer-left">
        
              <p class="footer-links">
                <a class="link-1" href="onama.php">O nama</a>
        
                <a href="kontakt.php">Kontakt</a>
        
                <a href="dostava.php">Dostava</a>
        
                <a href="povrat.php">Povrat i zamjena</a>
        
        
              </p>
        
              <p>SportsClothes &copy; 2022</p>
            </div>
        
          </footer>

     </body>
     </html>