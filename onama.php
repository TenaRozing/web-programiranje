<!DOCTYPE html>
<html lang="en">
<head>
<title>O nama</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/still.css">

<script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


</head>
<body style="background-color:white">

<?php include('navbar.php'); ?>

<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  margin: 0;

}

html {
  box-sizing: border-box;
  
}

*, *:before, *:after {
  box-sizing: inherit;
}

.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 8px 8px;
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  margin: 65px;
  padding: 20px;
}

.about-section {
  padding: 25px;
  text-align: center;
  background-color: #ffe6f3;
  color: #800008;
  
}

.container {
  padding: 16px 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}



@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.image {
  opacity: 1;
  display: block;
  transition: .5s ease;
  backface-visibility: hidden;
  
}


.slika_outleta:hover .image {
  opacity: 0.3;
}

.slika_outleta:hover .middle {
  opacity: 1;
}


</style>


<div class="about-section">
  <h1>O nama</h1>
  <p><br>Ovo je naš tim glavnih ljudi.</p>
  <p>Mali krug ljudi koji radi s ljubavlju i entuzijazmom.</p>
</div>

<br>

<h2 style="text-align:center">NAŠ TIM</h2>
<div class="row">
  <div class="column">
    <div class="card">
      <img src="slike/ana.jpg" alt="Jane" style="width:100%">
      <div class="container">
        <h2>Ana Ivić</h2>
        <p class="title">Direktorica</p>
       
        <p>ana.ivic@gmail.com</p>
       
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="slike/ferdo.jpg" alt="Mike" style="width:100%">
      <div class="container">
        <h2>Ferdo Jurić</h2>
        <p class="title">Voditelj nabave</p>
        
        <p>ferdo.juric@gmail.com</p>
        
      </div>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
      <img src="slike/ivan.jpg" alt="John" style="width:100%">
      <div class="container">
        <h2>Ivan Horvat</h2>
        <p class="title">Marketing</p>
        
        <p>ivan.horvat@gmail.com</p>
      
      </div>
    </div>
  </div>
</div>



<ul class="breadcrumb">
  <li><a href="index.php">Početna</a></li>
  <li>O nama</li>
</ul>

<br>
<div class="slika_outleta">
  <img src="slike/outlet.jpg" alt="Outlet" class="image" style="margin-left: 28px; margin-right: 40px;" width="95.5%" height="100%">
 
</div>
<br>
<div class="onama">
  <br>
<p>Tvrtka SportsClothes osnovana je 2020. godine s velikim entuzijazmom i ljubavlju prema sportu te se može reći da je jedna dosta mlada tvrtka.
  Ime tvrke nismo osmišljavali dugo jer je ovako dovoljno originalno i govori čime se ova tvrtka bavi, 
  a to je sportska moda.
  Tko kaže da sportska odjeća ne može biti lijepa i elegantna? Može! I to upravo uz SportsClothes. Uz nas uvijek budite i trendui u korak sa 
  zadnjim trendovima. U našoj ponudi možete pronaći razne marke kao primjerice Nike, Adidas, Puma i sl. Naša tvrtka nudi Vam mogućnost kupovine online i u nekim od naših prodajnih mjesta diljem Hrvatske,
  stoga što čekate? Naručite već sada komad za Vas ili posjetite neku od naših poslovnica navedenih dolje!</p> </div><br>
  <p class="radnje">Imamo 24 prodajnih mjesta  u Hrvatskoj <a href="radnje.php"> - pogledajte naše lokacije:</a></p><br>
  <style>
    .radnje{
      margin-left: 30px;
    }

    .radnje a:hover {
  background-color: #f1f1f1;
  color: #800000;
  text-decoration: none;
}

.radnje a:link {
  color: #800000;
  text-decoration: none;
}

.radnje a:visited {
  color: #800000;
  text-decoration: none;
}
  </style>
  <ul>
    <li>1 NIKE SHOP</li>
    <li>3 ADIDAS SHOP-ova</li>
    <li>4 MULTIBRAND TRGOVINA SportsClothes</li>
    <li>1 OUTLET</li>
  </ul> 
    
   <div class="info">
  <p>SportsClothes zapošljava više od 190 stalno zaposlenih djelatnika te 20-ak studenata koji svi svojim timskim radom i dogovorom ostvaruju uspješne rezultate i ciljeve jer kupac je najvažniji posjetitelj u
     našim prodavaonicama. On ne ovisi o nama, mi ovisimo o njemu. 
     On nije smetnja našem radu on je njegova svrha. Mi ne činimo uslugu služeći ga,
      on čini uslugu nama da ga služimo.</p>
    </div>
  </div>
  
    <ul>
      <li>SportsClothes d.o.o.</li>
      <li>Psunjska 188, 31000 Osijek</li>
      <li>OIB: 46782345678</li>
      <li>tel.: 031-272-555</li>
      <li>e-mail: info@sportsclothes.hr</li>
    </ul>
  
<br><br>
<footer class="footer-distributed">

    <div class="footer-right">

      <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
      <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
      <a href="https://www.pinterest.com/ferivisport"><i class="fa fa-pinterest"></i></a>
      

    </div>

  

    <div class="footer-left">

      <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


      </p>

      <p>SportsClothes &copy; 2022</p>
    </div>

  </footer>


 
 
