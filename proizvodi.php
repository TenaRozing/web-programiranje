<?php

include_once 'spoj.php';
session_start();
    if ($_SESSION['uloga'] != "admin"){
        header('Location: index.php'); 
    } 
?>

<head>
    <title>Proizvodi</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    </style>
</head>

<body>

    <?php include('navbar.php'); ?>
    <header class="mb-5">
        <h1 class="text-center">Pregled proizvoda</h1>
    </header>
    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="mx-auto p-5 my-5">
                <a href="administracija.php"><button class="btn">Povratak</button></a> <a href="unesiProizvode.php"><button class="btn" role="button">Dodaj <i class="fas fa-arrow-right"></i></button></a>
                    <br>
                    <br>
                    <table class="table table-light table-hover" id="tablica" style="max-height: 100px; height:100%;">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Marka</th>
                                <th scope="col">Naziv</th>
                                <th scope="col">Velicina</th>                            
                                <th scope="col">Cijena(kn)</th>
                                <th scope="col">Kolicina</th>
                                <th scope="col">Tip</th>
                                <th scope="col">Slika</th>
                                <th scope="col">Opcije</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        $sql = "SELECT * FROM proizvodi";
                        $q = mysqli_query($conn, $sql);

                        while($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
                            echo '
                                    <tr>
                                        <td >' . $line['id'] .'</td>
                                            <td>'. $line['marka'] . '</td>
                                            <td>'. $line['naziv'] .'</td>
                                            <td>'. $line['velicina'] .'</td>
                                            <td>'. $line['cijena'] .'</td>
                                            <td>'. $line['kolicina'] .'</td>
                                            <td>' .$line['tip'] . '</td>
                                            <td>' .$line['slika'] . '</td>

                                            <td>
                                                <a class="btn btn-sm" href="urediProizvode.php?id=' . $line['id'] .'">Uredi</a>
                                                <a class="btn btn-sm" href="obrisiProizvod.php?id=' . $line['id'] . '">Obriši</a>
                                            </td>
                                    </tr>';
                        }

                    echo' </tbody> </table>';
                            $conn->close();
                            ?>


                </div>
            </div>
        </div>
    </main>
    <script>
    $(document).ready(function() {
        $('#tablica').DataTable();
    });
    </script>

    <footer class="footer-distributed">

        <div class="footer-right">

            <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
            <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


        </div>

        <div class="footer-left">

            <p class="footer-links">
                <a class="link-1" href="onama.php">O nama</a>

                <a href="kontakt.php">Kontakt</a>

                <a href="dostava.php">Dostava</a>

                <a href="povrat.php">Povrat i zamjena</a>


            </p>

            <p>SportsClothes &copy; 2022</p>
        </div>

    </footer>


</body>

</html>


<style>
.btn{
    background-color:pink;

}

</style>