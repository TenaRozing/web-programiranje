<?php
session_start();
if(!isset($_SESSION['id'])){
    header('Location: index.php');
}
include_once 'spoj.php';

$user_id = $_SESSION['id'];
$sql = "SELECT id FROM kosarica WHERE korisnik_id = $user_id";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        $kosarica_id = $row['id'];
    }
}

$sql = "SELECT count(id) as total FROM stavka WHERE kosarica_id = $kosarica_id"; 

$br_stavki = mysqli_query($conn, $sql);
$data=mysqli_fetch_assoc($br_stavki);
if ($data['total'] == 0) {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}


$errorMsg = "";

$ukupnoNovac = 0;

if (isset($_POST['buy'])) {
    $mjesto = $_POST['mjesto'];
    $postanski_broj = $_POST['postanski_broj'];
    $adresa = $_POST['adresa'];
    $kucni_broj = $_POST['kucni_broj'];
    $query_narudzba = "INSERT INTO narudzbe (korisnik_id, mjesto_dostave, postanski_br, adresa, kucni_br) VALUES ($user_id, '$mjesto', $postanski_broj, '$adresa', $kucni_broj)";
    //die($query_narudzba);
    
    mysqli_query($conn, $query_narudzba);
    $last_id = mysqli_insert_id($conn);
    $sql4 = "SELECT * FROM stavka WHERE kosarica_id = $kosarica_id";
    $result1 = mysqli_query($conn, $sql4);
    if (mysqli_num_rows($result1) > 0) {
        while($row = mysqli_fetch_assoc($result1)) {
            $stavkaID = $row['id'];
            $naziv = $row['naziv'];
            $cijena = $row['cijena'];
            $kolicina = $row['kolicina'];
            $ukupnoNovac += $cijena * $kolicina;
            $sql5 = "INSERT INTO narudzba_stavke (naziv, cijena, kolicina, user_id, narudzba_id) VALUES ('$naziv', $cijena, $kolicina, $user_id, $last_id)";
            mysqli_query($conn, $sql5);
        }
       
    }
    
    if (isset($last_id)) {
        $sql6  = "UPDATE narudzbe SET ukupna_cijena = $ukupnoNovac WHERE id = $last_id";
        mysqli_query($conn, $sql6);
        header("Location: kupovina.php");
    } else {
        echo "Error: " . $sql6 . ":-" . mysqli_error($conn);
    }
    mysqli_close($conn);    
    }   
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kosarica</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/still.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/> 
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="jQueryFunkcije.js"></script>
</head>

<body>
    <?php include('navbar.php'); ?>
    <ul class="breadcrumb">
    <li><a href="index.php">Početna</a></li>
    <li>Košarica</li>
  </ul>
        <br><br>
        <header class="mb-5"><h1 class="text-center">Košarica</h1></header>
        <main>  
            <p id="dohvatiId"></p>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Adresa za dostavu</h5>
                            </div>
                            <div class="card-body">
                            
                                <input class="m-2" form="checkout_form" id="mjesto" name="mjesto" type="text" required placeholder="Mjesto">
                                <input class="m-2" form="checkout_form" type="number" id="postanski_broj" name="postanski_broj" required placeholder="Poštanski broj">
                                <input class="m-2" form="checkout_form" id="adresa" name="adresa" type="text" required placeholder="Adresa">
                                <input class="m-2" form="checkout_form" type="number" id="kucni_broj" name="kucni_broj" required placeholder="Kućni broj">
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                    <div id="kosarica_body" class="mx-auto p-5 my-5">
                        <form id="checkout_form" action="" method="post">
                            <table class="table table-light table-hover" id="cartTable" style="max-height: 100px; height:100%;">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Naziv</th>
                                        <th scope="col">Količina</th>
                                        <th scope="col">Cijena(kom)</th>
                                        <th scope="col">Cijena(ukupno)</th>
                                        <th scope="col">Ukloni</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $ukupno = 0;


                                    $user_id = $_SESSION['id'];
                                    $sql2 = "SELECT id FROM kosarica WHERE korisnik_id = $user_id";
                                    $result2 = mysqli_query($conn, $sql2);
                                    if (mysqli_num_rows($result2) > 0) {
                                        while($row = mysqli_fetch_assoc($result2)) {
                                            $kosarica_id = $row['id'];
                                        }
                                    }
                                    
                                    $sql3 = "SELECT * FROM stavka WHERE kosarica_id = $kosarica_id";
                                    $stavke_res = mysqli_query($conn, $sql3);
                                    
                                    $cartPrice = 0;
                                    $cartQuantity= 0;
                                    while($row = mysqli_fetch_assoc($stavke_res)) {
                                        $cartPrice += $row['cijena']*$row['kolicina'];
                                        $cartQuantity += $row['kolicina'];

                                        echo '
                                            <tr id="stavka_'.$row['id'].'">
                                                <td>' . $row['id'] .'</td>
                                                <td>' . $row['naziv'] .'</td>
                                                <td>' . $row['kolicina'] . '</td>
                                                <td>' . $row['cijena'] .'</td>
                                                <td class="total_kn">' . round($row['cijena']*$row['kolicina'],2) .'</td>
                                                <td class="'.$row['id'].'"><span id="stavka_'.$row['id'].'"><span>
                                                    <span onClick="ukloniIzKosarice(\'' .$row['id']. '\');" class="btn btn-danger">x</span>
                                                </td>
                                            </tr>';
                                    }

                                    
                                     echo '<tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td id="total_amount">Ukupno: '. round($cartPrice,2).' kn</td>
                                            <td></td>
                                        </tr>' ;?>
                                </tbody>
                            </table> 
                           
                                
                        <button type="submit" class="btn btn-success" name="buy">Kupi</button>
                          
                        </form>
                    </div>
                    </div>
                </div>

            </div>
        </main>
        <footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>

<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>

</footer>
</body>
</html>