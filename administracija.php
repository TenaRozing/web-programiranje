<?php

include_once 'spoj.php';
session_start();
    if ($_SESSION['uloga'] != "admin"){
        header('Location: index.php'); 
    } 
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Administracija</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="jQueryFunkcije.js"></script>
</head>

<body>

<?php include('navbar.php'); ?>
<ul class="breadcrumb">
    <li><a href="index.php">Početna</a></li>
    <li>Administracija</li>
  </ul>
<br>
    <header class="mb-5"><h1 class="text-center">Admin Dashboard</h1></header>
   
<main>
      <div class="container-fluid">
          <div class="row">
                <div class="mx-auto p-5 my-5 text-center">
                <table class="table table-light table-hover" id="tablica" style="max-height: 100px; height:100%;">
                        <thead>
                            <tr>
                            <th scope="col">Upravljanje korisnicima</th>
                            <th scope="col">Upravljanje artiklima</th>
                            </tr>
                        </thead>

                        <tbody>
                        <tr>
                           <td>
                           <a href="korisnici.php"><button class="btn">Upravljaj korisnicima <i class="fas fa-arrow-right"></i></button></a>
                            </td>
                            <td>
                           <a href="proizvodi.php"><button class="btn">Upravljaj artiklima <i class="fas fa-arrow-right"></i></button></a>
                            </td>
                        </tr>
                            </tbody>
                        </table>
                </div>
          </div>
      </div>
</main> 
<script>
$(document).ready(function() {
    $('#tablica').DataTable();
} );
</script>

<style>
    .btn {
        border: none;
        outline: none;
        padding: 12px 16px;
        background-color: pink;
        cursor: pointer;
        color: black;

    }

    .btn:hover {
        background-color: #ff6666;
        color: white;
    }

    .btn.active {
        background-color: #ff4d4d;
        color: white;
    }
</style>

<footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>

<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>
</footer>
</body>
</html>