<?php
session_start();
if(!isset($_SESSION['id'])){
    header('Location: index.php');
}
include_once 'spoj.php';


  
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kupovina</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>  
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="jQueryFunkcije.js"></script>
</head>


<body>
<?php include('navbar.php'); ?>
<ul class="breadcrumb">
    <li><a href="index.php">Početna</a></li>
    <li>Moje narudžbe</li>
  </ul>
<br>
<main>
<div id="artikliDiv" class="card">
    <span onClick="hideItems();" id="hideItems">x</span>
            <div class="card-header">
                <h5>Artikli</h5>
            </div>
            <div id="artikliList" class="card-body">

            </div>
        </div>
    <div class="container-fluid">

        <div class="card" style="max-width: 1400px;">
            <div class="card-header">
            <h1 class="text-center">Moje narudžbe</h1>
            </div>
                       
            <div class="card-body">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Broj narudžbe</td>
                        <td>Vrijeme narudžbe</td>
                        <td>Ukupna cijena</td>
                        <td>Artikli</td>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $user_id = $_SESSION['id'];

                        $sql = "SELECT * FROM narudzbe WHERE korisnik_id = $user_id";
                        $result = mysqli_query($conn, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            while($row = mysqli_fetch_assoc($result)) {
                                $id = $row['id'];
                                $vrijeme = $row['vrijeme'];
                                $cijena = round($row['ukupna_cijena'],2);
                                echo '<tr>
                                <td>'.$id.'</td>
                                <td>'.date('d.m.Y H:i', strtotime($vrijeme)).'</td>
                                <td>'.$cijena.'</td>
                                <td><span onClick="showItems('.$id.');" class="btn btn-secondary">Prikaži artikle</span></td>
                                </tr>';
                            }
                        }
                    ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</main>

<br>

<footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>



<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>
</footer>
</body>
</html>