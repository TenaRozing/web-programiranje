<!DOCTYPE html>
<html lang="en">
<head>
<title>Kontakt</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/still.css">

<script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</style>
</head>
<body style="background-color:white">

<?php include('navbar.php'); ?>

  <ul class="breadcrumb">
    <li><a href="index.php">Početna</a></li>
    <li>Kontakt</li>
  </ul>

<div class="pitanja">
    <h2>Pitaj nas sve što te zanima!</h2>
    <p> Pošalji nam mail ili nas nazovi.</p> <br>

    <p><i class="fa fa-envelope" aria-hidden="true"></i> Pošalji mail<br>info@sportsclothes.hr</p><br>
    
    <p><i class="fa fa-phone" aria-hidden="true"></i> Nazovi<br>031-272-555</p>

</div>

<br><br><br><br>

<footer class="footer-distributed">

    <div class="footer-right">

      <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
      <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
      <a href="https://www.pinterest.com/ferivisport"><i class="fa fa-pinterest"></i></a>
      

    </div>

    <div class="footer-left">

      <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


      </p>

      <p>SportsClothes &copy; 2022</p>
    </div>

  </footer>
 
