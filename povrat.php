<!DOCTYPE html>
<html lang="en">
<head>
<title>Povrat</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/still.css">

<script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</style>
</head>
<body style="background-color: white">

<?php include('navbar.php'); ?>


<ul class="breadcrumb">
        <li><a href="index.php">Početna</a></li>
        <li>Povrat i zamjena</li>
    </ul>


<div class="povrat">
    <h2>Povrat i zamjena robe</h2><br>
    <p>Prema Zakonu o zaštiti potrošača, članku 72, svaki je potrošač u mogućnosti raskinuti ugovor sklopljen izvan poslovnih prostorija ili sklopljen na daljinu u roku od 14 dana.</p><br>
    <p>
        Povrat ili zamjena robe je moguća u slučaju kada sa naručenih artikala niste skidali originalne etikete i kada posjedujete originalne kutije u kojima su artikli isporučeni.
        Prilikom povrata tenisica po kutiji tenisica se ne smije ništa lijepiti ili pisati,
        te iste ne smiju biti nošene van kuće, jer takve artikle nećemo prihvatiti za zamjenu ili povrat. Proizvod se ne smije  samo poslati u originalnoj kutiji,
        nego se mora staviti u dodatnu ambalažu, u suprotnom zamjena neće moći biti napravljena zbog oštećenja kutije.</p><br>
    <p>Naručene artikle je moguće zamijeniti za isti artikl druge veličine ili drugi artikl (ili artikle) u roku 14 dana od zaprimanja paketa.
       Također, ukoliko želite, možemo Vam plaćena sredstva vratiti na Vaš tekući račun (kod povrata paketa priložiti kopiju kartice tekućeg računa).
       U slučaj kada je paket plaćen kreditnom karticom, povrat sredstava se odvija na način da se kartičarskoj kući šalje zahtjev za storniranje transakcije.</p><br>
    <p>Prilikom plaćanja PayPal-om povrat sredstava je moguć unutar 60 dana od obavljanja kupnje. U slučaju povrata, sredstva se vraćaju na Vaš PayPal račun.
       Trošak dostave povrata artikla snosi kupac, dok trošak slanja zamjenskog artikla snosi SportsClothes d.o.o. 
       Prije svakog povrata ili zamjene robe, molimo Vas da nas kontaktirate na broj telefona 031-272-555 ponedjeljak-petak od 08:00 do 16:00 h.</p><br>
    <p><strong>Svi registrirani kupci imaju pravo na besplatan povrat i zamjenu. Također, rok za zamjenu i povrat artikala smo produžili na 30 dana od dana kupnje.</strong></p><br>
    <hr>
    <h2>Reklamacije</h2><br>
    <p>U slučaju reklamacije kupac je dužan vratiti nam proizvod u dogovorenom roku. 
       Reklamaciju ćemo smatrati valjanom ako pregledom proizvoda utvrdimo da odgovara uvjetima za reklamaciju sukladno Zakonu o obveznim odnosima i Zakonu o zaštiti potrošača, 
       <strong>te ukoliko nije prošlo više od 6 mjeseci od kupnje proizvoda.</strong> Kupac je dužan imati račun za proizvod koji reklamira, te originalnu ambalažu proizvoda. 
       U tom slučaju ćemo u roku od 15 dana od primitka valjane reklamacije zamijeniti proizvod.</p>
    <p>Obrazac možete preuzeti<a href="https://ferivisport.hr/media/PAGE/OBRAZAC_O_ZAMJENI-POVRATU_ARTIKALA2.pdf"> OVDJE. </a></p><br>

        
</div>

<footer class="footer-distributed">

    <div class="footer-right">

      <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
      <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
      <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>
      

    </div>

    <div class="footer-left">

      <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


      </p>

      <p>SportsClothes &copy; 2022</p>
    </div>

  </footer>
 