<?php
if (!PHP_SESSION_ACTIVE) session_start();
include_once 'spoj.php';

$errorMsg = "";

$id = $_GET['id']; // get id through query string

$qry = mysqli_query($conn, "SELECT * FROM proizvodi WHERE id='$id'");

$data = mysqli_fetch_array($qry);


$kolicina = $cijena = null;
$marka = $naziv = $tip = $slika = $velicina = "";
$isFilled = true;

if (isset($_POST['uredi'])) { 
    $marka = $_POST['marka'];
    $naziv = $_POST['naziv'];
    $velicina = $_POST['velicina'];
    $cijena = $_POST['cijena'];
    $kolicina = $_POST['kolicina'];
    $tip = $_POST['tip'];
    $slika = $_POST['slika'];
    $errorMsg = "";


    if (empty($_POST['marka'])) {
        $errorMsg .= "Marka obavezna. ";
        $isFilled = false;
    }
    else $marka = $_POST['marka'];

    if (empty($_POST['naziv'])) {
        $errorMsg .= "Naziv obavezan";
        $isFilled = false;
    }
    else $naziv = $_POST['naziv'];

    if (empty($_POST['velicina'])) {
        $errorMsg .= "Velicina obavezna";
        $isFilled = false;
    }
    else $velicina = $_POST['velicina'];

    if(empty($_POST['cijena']) or $_POST['cijena'] <= 0.00) {
        $errorMsg .= "Cijena mora biti veća od 0.";
        $isFilled = false;
    }
    else $cijena = $_POST['cijena'];
        
    if(empty($_POST['kolicina']) or $_POST['kolicina'] <= 0) {
        $errorMsg .= "Količina mora biti veća od 0!";
        $isFilled = false;
    }
    else $kolicina = $_POST['kolicina'];
    
    if (empty($_POST['tip'])) {
        $errorMsg .= "Tip obavezan";
        $isFilled = false;
    }
    else $tip = $_POST['tip'];

    if (empty($_POST['slika'])) {
        $errorMsg .= "Slika obavezan";
        $isFilled = false;
    }
    else $slika = $_POST['slika'];


    if($isFilled) {
        $sql = "UPDATE proizvodi set marka='$marka', naziv='$naziv', velicina='$velicina', cijena='$cijena', kolicina='$kolicina', tip='$tip', slika='$slika' WHERE id='$data[id]'";
        if (mysqli_query($conn, $sql)) {
            header('Location: proizvodi.php');
        } else {
            echo "Error: " . $sql . ":-" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }
}

?>


<head>
    <title>Web shop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    </style>
</head>

<body>
<?php include('navbar.php'); ?>
<br><br><br><br><br><br><br>
    <header class="mb-5"><h1 class="text-center">Uređivanje proizvoda</h1></header>
    <main>
        <div class="container-fluid mx-auto text-center">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <form method="post" action="" class="row">
                        <div class="row mb-3">
                        <div class="col-md-3">
                                <label for="marka" class="form-label">Marka</label>
                                <input type="text" name="marka" step="any" value="<?php echo $data['marka'] ?>" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="naziv" class="form-label">Naziv</label>
                                <input type="text" name="naziv" step="any" value="<?php echo $data['naziv'] ?>" class="form-control">
                            </div>

                            <div class="col-md-3">
                                <label for="velicina" class="form-label">Veličina</label>
                                <select class="form-select" name="velicina">
                                    <option value="S" <?php if($data['velicina']=="S") echo 'selected="selected"';?>>S</option>
                                    <option value="M" <?php if($data['velicina']=="M") echo 'selected="selected"';?>>M</option>
                                    <option value="L" <?php if($data['velicina']=="L") echo 'selected="selected"';?>>L</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="tip" class="form-label">Tip</label>
                                <select class="form-select" name="tip">
                                    <option value="Novo" <?php if($data['tip']=="Novo") echo 'selected="selected"'; ?> >Novo</option>
                                    <option value="Popularno"<?php if($data['tip']=="Popularno") echo 'selected="selected"'; ?>>Popularno</option>
                                    <option value="" <?php if($data['tip']=="") echo 'selected="selected"'; ?>>Ništa</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="cijena" class="form-label">Cijena</label>
                                <input type="number" name="cijena" step="any" value="<?php echo $data['cijena'] ?>" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="kolicina" class="form-label">Količina</label>
                                <input type="number" value="<?php echo $data['kolicina'] ?>" class="form-control" name="kolicina">
                            </div>
                            <div class="col-md-3">
                                <label for="slika" class="form-label">Slika</label>
                                <input type="file" value="<?php echo $data['slika'] ?>" class="form-control" name="slika">
                            </div>
                        </div>
                        
                        <div class="col-12 mt-3 mb-3">
                            <button type="submit" name="uredi" class="btn btn-success mx-1">Uredi</button>
                            <button type="reset" class="btn btn-warning mx-1">Resetiraj</button>
                            <a href="proizvodi.php" class="btn btn-info mx-1">Povratak</a>
                        </div>
                        <div class="my-2">
                            <p id="errorMsg"><?php echo $errorMsg ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</header>
<footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>

<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>
</footer>
</body>
</html>