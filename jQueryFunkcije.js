function toggle_cart(){
    if($('#kosarica_body:visible').length){
        $('#kosarica_body').hide();
        $('#toggle_btn').html("^");
    }else{
        $('#kosarica_body').show();  
        $('#toggle_btn').html("ˇ");  
    }
        
}

function dodajUKosaricu(id, naziv, cijena){
                
    $.ajax({
    url: "addToCart.php",
    type: "POST",
    data: {
        id: id,
        naziv: naziv,
        cijena: cijena
    },
    success: function(stored_id) {
        toastr["success"]("Artikl dodan u košaricu.");
        if($("#emptyCartSpan").length){
            $("#emptyCartSpan").remove();
            $("#total_cart_div").before('<table id="cartTable" class="table table-striped"><tr><td>Naziv</td><td>Cijena</td><td>Količina</td><td>Ukupno</td><td>Ukloni</td></tr></table>');
        }
        $('#cartTable').append(
            '<tr><td>'+naziv+'</td><td>'+cijena+'</td><td><input class="kolicina_input" type="number" value="1" min="1"></td><td class="total_kn">'+cijena+'</td><td class="'+stored_id+'"><span id="stavka_'+stored_id+'"><span onclick="ukloniIzKosarice('+stored_id+');" class="btn btn-danger">x</span></span></td></tr>'
        );
        var sum = 0;
        $(".total_kn").each(function(){
            sum += parseFloat($(this).html());
        });
        
        $('#total_amount').html(parseFloat(sum).toFixed(2));      
    },
    error: function(data) {
        toastr["error"]("Došlo je do pogreške, molim Vas da pokušate kasnije ili se javite se na mail tena_shop@mail.com .");
    }
    });
}

function ukloniIzKosarice(stavka_id){
                
    $.ajax({
    url: "removeFromCart.php",
    type: "POST",
    data: {
        stavka_id: stavka_id,
    },
    success: function(data) {
        toastr["success"]("Artikl uklonjen iz košarice.");
        $("#stavka_"+stavka_id).closest('tr').remove();
        if($("#cartTable tr").length == 1)
        {
            $('#cartTable').remove();    
            $('#kosarica_body').append("<span id='emptyCartSpan'>Vaša košarica je prazna.</span>");
        }
        var sum = 0;
        $(".total_kn").each(function(){
            sum += parseFloat($(this).html());
        });
        
        $('#total_amount').html(parseFloat(sum).toFixed(2));
    },
    error: function(data) {
        toastr["error"]("Došlo je do pogreške, molim Vas da pokušate kasnije ili se javite se na mail tena_shop@mail.com .");
    }
    });
}

$( document ).ready(function(){

    $(document).on("change keyup", ".kolicina_input", function(){
        var cijena = parseFloat($(this).closest('tr').find('td:nth-child(2)').html());
        var kolicina = parseFloat($(this).val());
        $(this).closest('tr').find('td:nth-child(4)').html(parseFloat(cijena*kolicina).toFixed(2));
        
        $('#zabiljezi_kolicine_btn').prop("disabled", false);
        var sum = 0;
        $(".total_kn").each(function(){
            sum += parseFloat($(this).html());
        });
        
        $('#total_amount').html(parseFloat(sum).toFixed(2));
    });    
})

function updateKolicine(){
    var stavke = [];
    var stavka = {};
    var rows = $("#cartTable tr").slice(1);
    $("#toCheckout").addClass("disable-click");
    rows.each(function(){
        stavka = {
            id : $(this).find('td:last-child').attr("class"),
            kolicina: $(this).find('.kolicina_input').val()
        }
        stavke.push(stavka);
        console.log(stavka);
    });
    $.ajax({
        url: "updateCartAmounts.php",
        type: "POST",
        data: {
            stavke: stavke
        },
        success: function(response) {
            $("#toCheckout").removeClass("disable-click");
            toastr["success"]("Količine odabranih artikala su zabilježene.");
        },
        error: function(data) {
            $("#toCheckout").removeClass("disable-click");
            toastr["error"]("Došlo je do pogreške, molim Vas da pokušate kasnije ili se javite se na mail tena_shop@mail.com .");
        }
    });
}

function showItems(id){
    $.ajax({
        url: "showOrderedItems.php",
        type: "GET",
        data: {
            id: id
        },
        success: function(response) {
            $('#artikliList').html(response);
            
            $('#artikliDiv').css("opacity", "1");
            $('#artikliDiv').css("z-index", "10");
        },
        error: function(data) {
            toastr["error"]("Došlo je do pogreške, molim Vas da pokušate kasnije ili se javite se na mail tena_shop@mail.com .");
        }
        });
}

function hideItems(){
    $('#artikliDiv').css("opacity", "0");
    $('#artikliDiv').css("z-index", "-1");
}