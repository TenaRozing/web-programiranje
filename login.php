<?php
include_once 'spoj.php';

$msg = 'Unesite vaše podatke.';

session_start();
$_SESSION['email'] = "";

if (isset($_POST['login']) && !empty($_POST['ime']) && !empty($_POST['password'])) {
    $sql = "SELECT * FROM korisnici";
    $q = mysqli_query($conn, $sql);

    while ($line = mysqli_fetch_array($q, MYSQLI_ASSOC)) {
        if ($_POST['ime'] == $line['ime'] && $_POST['password'] == $line['password']) {
            $_SESSION['loggedin'] = true;
            $_SESSION['id'] = $line['id'];
            $_SESSION['email'] = $_POST['email'];
            $_SESSION['uloga'] = $line['uloga'];
            $_SESSION['ime'] = $line['ime'];
               
            header('Location: index.php');    
        }
        else {
            $msg = 'Uneseni podaci su krivi.';
        }
    }
}
if(!empty($_POST["zapamti"])) {
	setcookie ("lozinka",$_POST["password"],time()+ 3600);
	
} else {
	setcookie("ime","");
	setcookie("password","");
	
}
?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <style>
    body {
        font-family: 'Oswald', sans-serif;
    }

    form {
        border: 3px solid #f1f1f1;
    }

    input[type=text],
    input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        box-sizing: border-box;
    }

    button {
        background-color: #800000;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    button:hover {
        opacity: 0.8;
    }

    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #800000;
    }

    .container {
        padding: 16px;

    }

    span.psw {
        float: right;
        padding-top: 16px;
    }


    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
        span.psw {
            display: block;
            float: none;
        }

        .cancelbtn {
            width: 100%;
        }
    }
    </style>
</head>

<body>


    <h2>Prijava </h2>
    <form method="post">
        <div class="container">
        <label for="ime"><b>Vaše ime:</b></label>
            <input type="text" placeholder="Upišite ime" name="ime">

            <label for="password"><b>Lozinka</b></label>
            <input type="password" placeholder="Upišite lozinku" name="password">

            <button type="submit" name="login">Login</button>
            <label>
                <input type="checkbox" checked="checked" name="zapamti"> Upamti me
            </label>
        </div>

        <div class="container" style="background-color:#f1f1f1">
            <a href="index.php"><button type="button" class="cancelbtn">Cancel</button></a> 
        </div>
    </form>
</body>

</html>