<?php 

include_once 'spoj.php';
session_start();


$error = "";
$errorPass = "";
$errorPassRepeat = "";
$success ="";


$ime = $email = $password = $repeatedPass = $uloga = "";
$status = true;

if (isset($_POST['register'])){ 
    $ime = $_POST['ime'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $repeatedPass = $_POST['repeated_password'];
    $uloga = "korisnik";
    $error = "";
    $errorPass = "";
    $errorPassRepeat = "";
    $loggedin = "";
    $success = "";

    if($password != $repeatedPass) {
        $error = "Lozinke se ne podudaraju!";
        $status = false;
    }

    else {
        $userExists = "SELECT * FROM korisnici WHERE email='$email'";
        $doesExist = mysqli_query($conn, $userExists);
        if (mysqli_num_rows($doesExist) > 0) {
            $error = "Email već zauzet!";
            $status = false;
        }

        if ($status) {
            $query = "INSERT INTO korisnici (ime, email, password, uloga) VALUES ('$ime', '$email', '$password', '$uloga')";
            $results = mysqli_query($conn, $query);
            if (!$results) {
                printf("Error: %s\n", $conn->error);
            }else{
                $last_id = mysqli_insert_id($conn);
                $query = "INSERT INTO kosarica (korisnik_id) VALUES ($last_id)";
                $results = mysqli_query($conn, $query);
            }
            

            $_SESSION['loggedin'] = true;
            $_SESSION['email'] = $email; 
            $_SESSION['uloga'] = $uloga;
            $_SESSION['ime'] = $ime;    
            $_SESSION['id'] = $last_id;                                 
            header('Location: index.php');
        }
    }

    
}

?>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    <style>
    body {
        font-family: 'Oswald', sans-serif;
        background-color: black;
    }

    * {
        box-sizing: border-box;
    }

    
    .container {
        padding: 16px;
        background-color: white;
    }

    
    input[type=text],
    input[type=password] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=text]:focus,
    input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }
    input[type=email]{
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }
    /* Overwrite default styles of hr */
    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    /* Set a style for the submit button */
    .registerbtn {
        background-color: #800000;
        color: white;
        padding: 16px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    .registerbtn:hover {
        opacity: 1;
    }

    /* Add a blue text color to links */
    a {
        color: #800000;
    }

    /* Set a grey background color and center the text of the "sign in" section */
    .signin {
        background-color: #f1f1f1;
        text-align: center;
    }
    </style>
</head>

<body>

    <form method="post">
        <div class="container">
            <h1>Registracija</h1>
            <p class="text-danger"><?php if($error) echo $error;?></p>
            <hr>
            <label for="ime"><b>Ime</b></label>
            <input required type="text" placeholder="Upišite ime" name="ime" id="ime">

            <label for="email"><b>Email</b></label>
            <input required type="email" placeholder="Upišite email" name="email" id="email">

            <label for="password"><b>Lozinka</b></label>
            <input required type="password" placeholder="Upišite lozinku" name="password" id="password">

            <label for="repeated_password"><b>Ponovno upišite lozinku</b></label>
            <input required type="password" placeholder="Ponovno upišite lozinku" name="repeated_password"
                id="repeated_password">
            <hr>


            <button type="submit" class="registerbtn" name="register">Registriraj se</button>
          
        </div>

        <div class="container signin">
            <p>Već imate račun? <a href="login.php">Login</a></p>
        </div>
    </form>
</body>

</html>