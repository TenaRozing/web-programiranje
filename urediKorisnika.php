<?php

include 'spoj.php';
session_start();

$errorMsg = "";

$id = $_GET['id']; // get id through query string

$qry = mysqli_query($conn, "SELECT * FROM korisnici WHERE id='$id'");

$data = mysqli_fetch_array($qry);

$ime = $email = $uloga= "";
$isFilled = true;

if(isset($_POST['uredi'])) {

    if (empty($_POST['ime'])) {
        $errorMsg .= "Ime obavezno. ";
        $isFilled = false;
    }
    else $ime = $_POST['ime'];

    if (empty($_POST['email'])) {
        $errorMsg .= "Email obavezan. ";
        $isFilled = false;
    }
    else $email = $_POST['email'];

    if (empty($_POST['uloga'])) {
        $errorMsg .= "Uloga obavezna. ";
        $isFilled = false;
    }
    else $uloga = $_POST['uloga'];


    if($isFilled) {
        $sql = "UPDATE korisnici SET ime='$ime', email='$email', uloga='$uloga' WHERE id='$data[id]'";
        if (mysqli_query($conn, $sql)) {
            header('Location: korisnici.php');
        } else {
            echo "Error: " . $sql . ":-" . mysqli_error($conn);
        }
        mysqli_close($conn);
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Web shop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="jQueryFunkcije.js"></script>
</head>

<body>
<?php include('navbar.php'); ?>
<br><br><br>
<br><br><br>


<header class="mb-5"><h1 class="text-center">Uređivanje korisnika</h1></header>
 
<main>
    <div class="container-fluid text-center ">
        <div class="row">
             <div class="col-md-9 mx-auto ">
             <form method="post" action="" class="row">
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="ime" class="form-label">Ime</label>
                                <input type="text" name="ime" class="form-control" value="<?php echo $data['ime'] ?>"autofocus>
                            </div>
                            <div class="col-md-6">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" name="email" class="form-control" value="<?php echo $data['email'] ?>">
                            </div>
                            <div class="col-md-6"><br> 
                                <label for="uloga" class="form-label">Uloga(admin/korisnik)</label>
                                <select class="form-select" name="uloga">
                                    <option value="admin" <?php if($data['uloga']=="admin") echo 'selected="selected"'; ?>>Admin</option>
                                    <option value="korisnik" <?php if($data['uloga']=="korisnik") echo 'selected="selected"'; ?>>Korisnik</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 mt-3 mb-3">
                            <button type="submit" name="uredi" class="btn btn-success mx-1">Uredi</button>
                            <button type="reset" class="btn btn-warning mx-1">Očisti</button>
                            <a href="korisnici.php" class="btn btn-info mx-1">Povratak</a>
                        </div>
                        <div class="my-2">
                            <p id="errorMsg"><?php echo $errorMsg ?></p>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</main><br> 
<footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>

<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>
</footer>
</body>
</html>