<!DOCTYPE html>
<html lang="en">
<head>
<title>Dostava</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/still.css">

<script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</style>
</head>
<body style="background-color:white">

<?php include('navbar.php'); ?>

  <ul class="breadcrumb">
    <li><a href="index.php">Početna</a></li>
    <li>Dostava</li>
  </ul>

<div class="dostava">
    <h2>Dostava unutar Republike Hrvatske</h2><br>
    <p>Naručenu robu dostavljamo u roku 7 radnih dana od zaprimanja narudžbe.</p><br>
   
    <p>Dostava se ne naplaćuje za narudžbe koje prelaze 500,00 kn. Za sve narudžbe manje od 500,00 kn trošak dostave se naplaćuje u iznosu od 30 kn za Overseas Express,
        te 40 kn za GLS i DPD (prilikom narudžbe). Pakete dostavljamo dostavljačkim kućama Overseas Express,
        GLS i DPD (prema odabiru kupca prilikom narudžbe) od ponedjeljka do petka.
        Ukoliko niste kod kuće u vremenu kad dođe dostavljač, 
        dostavljač će Vas kontaktirati telefonski i dogovoriti ćete se oko termina dostave, ili će Vam ostaviti obavijest na adresi.</p><br>
     <hr>
        <h2>Dostava u trgovinu</h2><br>  
     <p>Naručenu robu Vam možemo dostaviti u neku od naših trgovina:<br>
        Nakon narudžbe paket ćemo poslati u trgovinu koju odaberete, te će Vas kolege kontaktirati na broj telefona koji ostavite prilikom narudžbe, kada paket bude sprema za isporuku.
        Kod narudžbi putem web shopa koje ćete preuzeti u našim trgovinama moguće je jedino plaćanje kreditnim karticama ili PayPal sustavom na web shopu.</p><br>
     <hr>
     <h2>Međunarodna dostava</h2> <br>
     <p>Proizvode van Hrvatske šaljemo DHL-om i Overseas Expressom (Eurodis), ovisno o dostavnoj službi koju ste odabrali u procesu narudžbe (kao i dostupnosti dostave pojedinom dostavnom službom u određenu državu). Rok isporuke ovisi o državi u koju šaljemo proizvod, kao i dostavnoj službi koju ste odabrali. Za zemlje izvan EU, ukoliko postoji trošak špedicije i carinjenja, trošak snosi kupac (u protivnom će paket biti uništen, bez povrata novaca).

        Zemlje za koje je dostupna dostava, kao i cijene dostave možete vidjeti u dolje navedenim tablicama, prema dostavnim službama Overseas Express i DHL. </p><br>
</div>

<footer class="footer-distributed">

    <div class="footer-right">

      <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
      <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
      <a href="https://www.pinterest.com/ferivisport"><i class="fa fa-pinterest"></i></a>
      

    </div>

    <div class="footer-left">

      <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>

      </p>

      <p>SportsClothes &copy; 2022</p>
    </div>

  </footer>