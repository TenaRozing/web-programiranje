<?php
session_start();



  
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kupovina</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="jQueryFunkcije.js"></script>
</head>


<body>
<?php include('navbar.php'); ?>
<br><br><br><br><br>
<header class="mb-5"><h1 class="text-center">Kupnja izvršena</h1></header>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 mx-auto mt-5 p-4 bg-dark text-muted text-center">
                <?php 
                    include_once 'spoj.php';
                    $user_id = $_SESSION['id'];
                    $sql = "SELECT id FROM narudzbe WHERE korisnik_id = $user_id ORDER BY id DESC LIMIT 1";
                    
                    
                    $sql = "SELECT id FROM kosarica WHERE korisnik_id = $user_id";
                    $result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                        while($row = mysqli_fetch_assoc($result)) {
                            $kosarica_id = $row['id'];
                        }
                    }
                    
                    $result = mysqli_query($conn, $sql);
                    while($data = mysqli_fetch_array($result)) {
                        $narudzba_id = $data['id'];
                    }
                    $sql = "SELECT * FROM narudzba_stavke WHERE narudzba_id = '$narudzba_id'";
                    $result = mysqli_query($conn, $sql);
                    echo '<h2 class="text-white">Hvala na kupnji!</h2>';
                    while($data = mysqli_fetch_array($result)) {
                        echo
                        '
                        <p>Naziv:  <span class="text-white">'.$data['naziv'].'</span></p>
                        <p>Količina:  <span class="text-white">'.$data['kolicina'].'</span></p>
                        <p>Cijena:  <span class="text-white">'.$data['cijena'].' kn</span></p>';
                    }

                    $deleteSQL = "DELETE FROM stavka where kosarica_id = $kosarica_id";
                    mysqli_query($conn, $deleteSQL);
                ?>
            </div>
        </div>
    </div>
</main>

<br>

<footer class="footer-distributed">

<div class="footer-right">

    <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
    <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


</div>



<div class="footer-left">

    <p class="footer-links">
        <a class="link-1" href="onama.php">O nama</a>

        <a href="kontakt.php">Kontakt</a>

        <a href="dostava.php">Dostava</a>

        <a href="povrat.php">Povrat i zamjena</a>


    </p>

    <p>SportsClothes &copy; 2022</p>
</div>
</footer>
</body>
</html>