<?php
session_start();
include_once 'spoj.php';
$id = $_GET['id'];

$user_id = $_SESSION['id'];
echo '<h3>Narudžba br '.$id.'</h3>';
$sql = "SELECT * FROM narudzba_stavke WHERE narudzba_id = $id";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
    echo '<table class="table table-striped">
    <thead>
        <tr>
            <td>Naziv</td>
            <td>Cijena</td>
            <td>Količina</td>
            <td>Ukupno</td>
        </tr>
    </thead>
    <tbody>';
    while($row = mysqli_fetch_assoc($result)) {
        $naziv = $row['naziv'];
        $cijena = $row['cijena'];
        $kolicina = $row['kolicina'];
        echo '<tr><td>'.$naziv.'</td><td>'.round($cijena,2).'</td><td>'.$kolicina.'</td><td>'.round($cijena*$kolicina,2).'</td></tr>';
    }
    echo '</tbody></table>';
}
  
  mysqli_close($conn);
?>
