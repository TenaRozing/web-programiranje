<head>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script>

         $(document).ready(function(){
             if(!localStorage.getItem('artikli')) $('.kosarica span').text(0);
             else $('.kosarica span').text(parseInt(localStorage.getItem('artikli')));            
            })
    </script>
</head>


<nav class="navbar navbar-expand-lg fixed-top" style="background-color: #ffccee; ">
    <div class="container-fluid">
      <a class="navbar-brand" style="font-family: 'Brush Script MT', cursive; color: #800008; font-size: 30.5px;" href="index.php" >SportsClothes</a> <p style="margin-top:15px;"><?php if(isset($_SESSION['ime'])) echo "Pozdrav, ", $_SESSION['ime']; ?>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">

          <li class="nav-item">
            <a class="nav-link" href="zenskaOdjeca.php" style="font-family: 'Oswald', sans-serif; color: #800000; font-size: 16px;">Pregled asortimana</a>
          </li>
          <?= 
            !isset($_SESSION['ime']) ?
            "<li class='nav-item'>
            <a class='nav-link active' aria-current='page' href='registracija.php' style='font-family:".'Oswald'.", sans-serif; color: #800000; font-size: 16px;'><i class='fa fa-user' aria-hidden='true'></i> Registracija</a>
          </li>":
          ''
          ?>
          <?=
          isset($_SESSION['ime']) ?
          "<li class='nav-item'>
            <a class='nav-link' href='odjava.php' style='font-family:". 'Oswald'.", sans-serif; color: #800000; font-size: 16px;'>Odjavi me</a>
          </li>"
          : ''
          ?>
          <?php if (isset($_SESSION['uloga']) && $_SESSION['uloga'] == "admin") { ?>
         
          
          <li class="nav-item">
            <a class="nav-link" href="administracija.php" style="font-family: 'Oswald', sans-serif; color: #800000; font-size: 16px;">Dashboard</a>
          </li>
        <?php } if(isset($_SESSION['id'])){?>
          <li class="nav-item">
            <a class="nav-link" href="kosarica.php" style="font-family: 'Oswald', sans-serif; color: #800000; font-size: 16px;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Košarica</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="moje_narudzbe.php" style="font-family: 'Oswald', sans-serif; color: #800000; font-size: 16px;"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Moje narudžbe</a>
          </li>
          <?php } ?>
        </ul>
    </div>
  </nav>