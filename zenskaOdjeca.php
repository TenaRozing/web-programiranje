<?php 
    session_start();
    include_once 'spoj.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Web shop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/still.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet"/>  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="jQueryFunkcije.js"></script>
</head>

<body>

<div id="kosarica" style="max-width: 420px; margin-left:auto;margin-right:auto;">
    <div style="width:100%; text-align:center;">
        <span onClick="toggle_cart();" id="toggle_btn">ˇ</span>
        <h5>Košarica</h5>
        <div id="kosarica_body">
        <?php
            $ukupno = 0;
            if(isset($_SESSION['id'])){
                $session_id =  $_SESSION['id'];
                $statement = "SELECT id from kosarica where korisnik_id = $session_id";
                $result = mysqli_query($conn, $statement);
                if ($result) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $kosarica_id = $row['id'];
                    }
                }
                $statement = "SELECT * FROM stavka WHERE kosarica_id = $kosarica_id";
                $result = mysqli_query($conn, $statement);
                if (mysqli_num_rows($result)) {
                    echo '<table id="cartTable" class="table table-striped">';
                    echo '<tr>
                    <td>Naziv</td>
                    <td>Cijena</td>
                    <td>Količina</td>
                    <td>Ukupno</td>
                    <td>Ukloni</td>
                    </tr>';
                    while($row = mysqli_fetch_assoc($result)) {
                        $stavkaID = $row['id'];
                        $naziv = $row['naziv'];
                        $cijena = $row['cijena'];
                        $kolicina = $row['kolicina'];
                        $ukupno += $cijena*$kolicina;
                        echo '<tr>
                        <td>'.$naziv.'</td>
                        <td>'.number_format($cijena,2, ".", "").'</td>
                        <td><input class="kolicina_input" type="number" value="'.$kolicina.'" min="1"></td>
                        <td class="total_kn">'.number_format($cijena*$kolicina,2, ".", "").'</td>
                        <td class="'.$stavkaID.'"><span id="stavka_'.$stavkaID.'"><span>
                            <span onClick="ukloniIzKosarice(\'' .$stavkaID. '\');" class="btn btn-danger">x</span>
                        </td>
                        </tr>';
                    }
                    echo '</table>';
                }else{
                    echo "<span id='emptyCartSpan'>Vaša košarica je prazna.</span>";
                }
            }else{
                echo "Prijavite se kako bi dodali artikle u košaricu.";
            }

        ?>
        <div id="total_cart_div">
            <span id="total_hrk_span">UKUPNO: <?php echo "<span id='total_amount'>".number_format($ukupno, 2, ".", "")."</span>"; ?> HRK</span>
            <div id="buttons_cart_div>">
                <button onClick="updateKolicine();" disabled id="zabiljezi_kolicine_btn" class="btn btn-success ml-5">Zabilježi količine</button>
                <a href="kosarica.php" id="toCheckout" class="btn btn-secondary ml-5">Završi kupovinu</a>
            </div>
            
        </div>
        </div>
        
    </div>
   
</div>
    <?php include('navbar.php'); ?>


    <ul class="breadcrumb">
        <li><a href="index.php">Početna</a></li>
        <li>Stranica s odjećom</li>
        <li><input type="text" name="search" id="search"><button id="searchBtn" class="btn btn-danger m-1">Pretraži</button></li>
    </ul>

    <div class="container-fluid">
        
        <div class="row g-2">
            <?php
                $sql = "SELECT marka, naziv, velicina, cijena, tip, id, slika FROM proizvodi WHERE tip='Novo' OR tip='Popularno'";
                $result = mysqli_query($conn, $sql);
                
                if (mysqli_num_rows($result) > 0) {
                    $index_id = 0;
                    while($row = mysqli_fetch_assoc($result)) {
                        
                        echo'
                            <div class="col-md-4">
                                    <div class="card" style="margin-left:50px;">
                                        <div class="img-container">
                                            <div class="d-flex justify-content-between align-items-center p-2 first" > </div>
                                            <img src="slike/'.$row['slika'].'" class="img-fluid"
                                        </div>
                                        <div class="product-detail-container">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <h6 class="mb-0">&nbsp;'.$row['naziv']. '<br><p class="text-danger font-weight-bold" style="margin-right:50px"><br>'.$row['cijena'].' kn</p>
                                            </div>
                                            <div class="d-flex justify-content-between align-items-center mt-2">

                                                <div class="size">&nbsp;Velicina: '.$row['velicina'].'</div>
                                            </div>
                                            <div class="mt-3 text-center">';
                                            if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
                                                echo '
                                                    <span class="dodajuKosaricu" onclick="dodajUKosaricu(\'' . $row['id'] . '\',\'' . $row['naziv']. '\',\'' . $row['cijena']. '\');"><button>Dodaj u košaricu</button></span>
                                                    <p>';
                                            }
                                            echo 
                                            '</div>
                                        </div>
                                    </div>
                                </div>
                            </div>';
    }
} else {
    echo "0 results";
}
mysqli_close($conn); ?>

        </div>
        <br>

        <style>
        #myBtn {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 30px;
            z-index: 99;
            font-size: 18px;
            border: none;
            outline: none;
            background-color: #800008;
            color: white;
            cursor: pointer;
            padding: 15px;
            border-radius: 4px;
        }

        #myBtn:hover {
            background-color: #d9d9d9;
            color: black;
        }

        .card {
            border: 1.5px solid #ff66ff;
            box-shadow: 0 4px 6px 0 rgba(22, 22, 26, 0.18);
  
 
        }
        </style>

        <button onclick="topFunction()" id="myBtn" title="Go to top">Povratak na vrh</button>

        <script>
        //Get the button
        var mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
        
            
        $('#searchBtn').on('click', function(){
                
            var inputVal = $("#search").val();
            $.ajax({
            url: "search.php",
            type: "GET",
            data: {
                search: inputVal,
            },
            success: function(data) {
                $('.container-fluid .row').html(data);
            },
            error: function(data) { 
                $('.container-fluid .row').html("Došlo je do pogreško, molimo pokušajte ponovno.");
            }
            });
        });
        

        </script>


        <br><br>
        <style>
        .izmjenabtn {
            margin-left: 120px;
            font-size: 18px;
            border-radius: 4px;
            border: none;
            background-color: #ff4d4d;
            padding: 12px 24px;
            color: white;
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        </style>


        <footer class="footer-distributed">

            <div class="footer-right">

                <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
                <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


            </div>

            <div class="footer-left">

                <p class="footer-links">
                    <a class="link-1" href="onama.php">O nama</a>

                    <a href="kontakt.php">Kontakt</a>

                    <a href="dostava.php">Dostava</a>

                    <a href="povrat.php">Povrat i zamjena</a>


                </p>

                <p>SportsClothes &copy; 2022</p>
            </div>
        </footer>
</body>

</html>