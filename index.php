<?php
    include_once 'spoj.php';
    session_start();
  ?>

<head>
    <title>Web shop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="css/still.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
    <script src="https://kit.fontawesome.com/903d8427c1.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>    
</head>

<body>

    <?php include('navbar.php'); ?>
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="slike/buzz.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>BUDITE UVIJEK U TRENDU.</h5>
                    <p>Uz najnovije kolekcije odjeće marki Adidas, Nike i mnoge druge!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="slike/slika1.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>BUDITE UVIJEK U TRENDU.</h5>
                    <p>Uz najnovije kolekcije odjeće marki Adidas, Nike i mnoge druge!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="slike/terex.jpg" class="d-block w-100" alt="...">
                <div class="carousel-caption d-none d-md-block">
                    <h5>BUDITE UVIJEK U TRENDU.</h5>
                    <p>Uz najnovije kolekcije odjeće marki Adidas, Nike i mnoge druge!</p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
            data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
            data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <img src="slike/slika3.png" alt="bijela" width="100%">
    
    <style>
    * {
        box-sizing: border-box;
    }

    .column {
        float: left;
        width: 33.33%;
        padding: 5px;
    }

    /* Clearfix (clear floats) */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }
    </style>

    <div class="row">
        <div class="column">
            <a href="dostava.php"><img src="slike/ferivi1.jpg" alt="Crvena" style="width:100%"></a>
        </div>
        <div class="column">
            <a href="kontakt.php"><img src="slike/ferivi2.jpg" alt="Crna" style="width:100%"></a>
        </div>
        <div class="column">
            <a href="dostava.php"><img src="slike/ferivi3.jpg" alt="Žuta" style="width:100%"></a>

        </div>
    </div>


    <style>
    * {
        box-sizing: border-box;
    }
  
    .body1 {
        background-color: #ffffcc;
        padding: 30px;
        font-family: Arial;
    }

    /* Center website */
    .main {

        margin: auto;
    }

    h1 {
        font-size: 50px;
        word-break: break-all;
        text-align: center;

    }

    h2 {
        text-align: center;
    }

    .row {
        margin: 10px -16px;
    }

    /* Add padding BETWEEN each column */
    .row,
    .row>.column {
        padding: 8px;
    }

    /* Create three equal columns that floats next to each other */
    .column {
        float: left;
        width: 33.33%;
        display: none;
        /* Hide all elements by default */
    }

    /* Clear floats after rows */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Content */
    .content {
        background-color: white;
        padding: 10px;
    }

    /* The "show" class is added to the filtered elements */
    .show {
        display: block;
    }

    /* Style the buttons */
    .btn {
        border: none;
        outline: none;
        padding: 12px 16px;
        background-color: white;
        cursor: pointer;
        color: black;

    }

    .btn:hover {
        background-color: #ff6666;
        color: white;
    }

    .btn.active {
        background-color: #ff4d4d;
        color: white;
    }
    </style>


    <!-- MAIN (Center website) -->
    <div class="body1">
        <div class="main">

            <h1>POGLEDAJTE NEŠTO OD NAŠE PONUDE</h1>
            <hr>

            <h2>UZ NAS BUDITE UVIJEK U KORAK S MODOM</h2>

            <br>

            <div id="myBtnContainer">
                <button class="btn active" onclick="filterSelection('all')"> Prikaži sve</button>
                <button class="btn" onclick="filterSelection('adidas')"> Adidas</button>
                <button class="btn" onclick="filterSelection('nike')"> Nike</button>
                <button class="btn" onclick="filterSelection('puma')"> Puma</button>
            </div>


            <div class="row">
                <div class="column adidas">
                    <div class="content">
                        <img src="slike/fashion1.jpg" alt="Adidas pink shirt" width="100%">
                        <h4>Adidas ružičasta majica</h4>
                        <p>Ide uz svake traperice</p>
                    </div>
                </div>
                <div class="column adidas">
                    <div class="content">
                        <img src="slike/fashion2.jpg" alt="Adidas brown shirt" width="100%">
                        <h4>Adidas svijetlo smeđi bodi</h4>
                        <p>Idete li u grad ili u teretanu?
                            Svejedno je, moderno i ležerno.
                        </p>
                    </div>
                </div>

                <div class="column adidas">
                    <div class="content">
                        <img src="slike/fashion3.jpg" alt="Adidas black shirt" width="100%">
                        <h4>Adidas crna klasična majica</h4>
                        <p>Klasika koja uvijek prolazi</p>
                    </div>
                </div>

                <div class="column nike">
                    <div class="content">
                        <img src="slike/nike1.jpg" alt="Nike shirt 1" width="100%">
                        <h4>Nike sweatshirt</h4>
                        <p>Za sve one koji prje svega vole udobnost</p>
                    </div>
                </div>
                <div class="column nike">
                    <div class="content">
                        <img src="slike/nike2.jpg" alt="Nike shirt 2" width="100%">
                        <h4>Nike crop top</h4>
                        <p>Trebate samo odlučiti koja vam je najdraža boja</p>
                    </div>
                </div>
                <div class="column nike">
                    <div class="content">
                        <img src="slike/nike3.jpg" alt="Nike shirt 3" width="100%">
                        <h4>Nike sive tajice</h4>
                        <p>Za prave sportašice</p>
                    </div>
                </div>

                <div class="column puma">
                    <div class="content">
                        <img src="slike/puma1.jpg" alt="Puma shirt 1" width="100%">
                        <h4>Puma dvobojne tajice</h4>
                        <p>Uvijek u trendu pa čak i u teretani</p>
                    </div>
                </div>
                <div class="column puma">
                    <div class="content">
                        <img src="slike/puma2.jpg" alt="Puma shirt 2" width="100%">
                        <h4>Puma narančasta majica</h4>
                        <p>Idealan poklon za tvoju prijateljicu</p>
                    </div>
                </div>
                <div class="column puma">
                    <div class="content">
                        <img src="slike/puma3.jpg" alt="Puma shirt 3" width="100%">
                        <h4>Puma komplet</h4>
                        <p>Od glave do pete - PUMA</p>
                    </div>
                </div>
                <!-- END GRID -->
            </div>

            <!-- END MAIN -->
        </div>
    </div>

    <script>
    filterSelection("all")

    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("column");
        if (c == "all") c = "";
        for (i = 0; i < x.length; i++) {
            w3RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
    }

    function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);
            }
        }
        element.className = arr1.join(" ");
    }


    // Add active class to the current button (highlight it)
    var btnContainer = document.getElementById("myBtnContainer");
    var btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
    </script>
    <style>
    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: #800008;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

    #myBtn:hover {
        background-color: #d9d9d9;
        color: black;
    }
    </style>

    <button onclick="topFunction()" id="myBtn" title="Go to top">Povratak na vrh</button>

    <script>
    //Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    </script>

    <footer class="footer-distributed">

        <div class="footer-right">

            <a href="https://www.facebook.com/Ferivi1994/"><i class="fa fa-facebook"></i></a>
            <a href="https://www.instagram.com/ferivi.hr/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="https://www.pinterest.com/ferivisport/"><i class="fa fa-pinterest"></i></a>


        </div>

        <div class="footer-left">

            <p class="footer-links">
                <a class="link-1" href="onama.php">O nama</a>

                <a href="kontakt.php">Kontakt</a>

                <a href="dostava.php">Dostava</a>

                <a href="povrat.php">Povrat i zamjena</a>


            </p>

            <p>SportsClothes &copy; 2022</p>
        </div>

    </footer>

</body>

</html>